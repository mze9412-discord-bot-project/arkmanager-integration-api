// Copyright (C) 2022 Mathias Zech
// This file is part of ArkManager Integration API <https://gitlab.com/mze9412-discord-bot-project/arkmanager-integration-api>.
//
// ArkManager Integration API is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkManager Integration API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkManager Integration API. If not, see <http://www.gnu.org/licenses/>.

import { exec } from "child_process";
import { Logger } from "tslog";
import { sleep } from "./waiting";

export class BashExecution {
    private static _logger: Logger;
    public static set logger(logger: Logger) { this._logger = logger; }
    public static get logger(): Logger { return this._logger; }

    private constructor() {
    }

    public static async executeCommand(command: string, readOutput: boolean): Promise<[number, string]> {
        const output: string[] = [];
        this._logger.info(`Execute: Starting command "${command}".`);
        const proc = exec(command, (error, stdout, stderr): void => {
                if (error) {
                    this._logger.error(`Execute Error: ${error}`);
                }

                if (readOutput) {
                    output.push(stdout);
                }
                this._logger.debug('Execute: STDOUT/STDERR.');
                this._logger.debug(stdout);
                this._logger.debug(stderr);
                this._logger.debug('Execute: STDOUT/STDERR. Done.');
            });

        let isFinished = false;
        proc.on('exit', (exitCode) => {
            this._logger.debug(`Execute: Exited command "${command}" with code "${exitCode}"`);
            isFinished = true;
        });

        // wait for process to exit
        while(!isFinished) {
            await sleep(100);
        }
        this._logger.info(`Execute: Finished command "${command}" with exit code ${proc.exitCode}.`);
        return [<number>proc.exitCode, output.join('\n')];
    }
}