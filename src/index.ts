// Copyright (C) 2022 Mathias Zech
// This file is part of ArkManager Integration API <https://gitlab.com/mze9412-discord-bot-project/arkmanager-integration-api>.
//
// ArkManager Integration API is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkManager Integration API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkManager Integration API. If not, see <http://www.gnu.org/licenses/>.

import express, { NextFunction } from 'express';
import responseTime from 'response-time';
import { BashExecution } from './bashExecution';
import { HttpException } from './httpException';
import { config as dotenv } from 'dotenv'
import https from 'https';
import fs from 'fs';
import { Logger } from 'tslog';

const logger = new Logger({name: 'ArkManagerIntegrationAPI'});
BashExecution.logger = logger.getChildLogger({name: 'BashExecution'});

dotenv();

const app = express();
app.use(express.json());

app.use(responseTime((req, res, time) => {
    const method = req.method;
    const url = req.url;

    if (method != undefined && url != undefined) {
        logger.info(`${method} ${url} - ${time}`);
    }
}));
app.use(async (err: HttpException, req: express.Request, res: express.Response, next: NextFunction): Promise<void> => {
    logger.error(err.stack);
    next(err);
});

const portEnv = process.env.ARKMAN_INTEGRATION_PORT;
const tokenEnv = process.env.ARKMAN_INTEGRATION_TOKEN;

if (portEnv == undefined || tokenEnv == undefined) {
    logger.error('Port or API token are undefined. Did you set the required environment variables ARKMAN_INTEGRATION_PORT and ARKMAN_INTEGRATION_TOKEN?');
    process.exit(1);
}

const port = Number.parseInt(portEnv);

// use in handlers
const apiToken = tokenEnv;

// command endpoint
app.post('/command/:apiToken', async(req, res) => {
    const token = req.params.apiToken;

    // token must match
    if (token !== apiToken) {
        await res.sendStatus(401);
        return;
    }

    // parse body to check response
    const data = req.body;
    const command = data.command;
    
    let serverName = data.serverName;
    if (serverName !== '') {
        serverName = ' @' + serverName;
    }

    const returnValue = {
        serverName: serverName,
        command: command,
        success: true,
        actionRequired: false
    }

    switch(command) {
        case 'start':
            await BashExecution.executeCommand(`arkmanager start${serverName}`, false);
        break;
        case 'stop':
            await BashExecution.executeCommand(`arkmanager stop${serverName}`, false);
        break;
        case 'restart':
            await BashExecution.executeCommand(`arkmanager restart${serverName}`, false);
        break;
        case 'update':
            await BashExecution.executeCommand(`arkmanager update --update-mods${serverName} > /dev/null`, false);
        break;
        case 'checkupdate': {
                const updateOutput = await BashExecution.executeCommand(`arkmanager checkupdate${serverName}`, true);
                const modUpdateOutput = await BashExecution.executeCommand(`arkmanager checkmodupdate${serverName}`, true);
                returnValue.actionRequired = updateOutput[1].indexOf('Your server needs to be restarted in order to receive the latest update.') !== -1 || modUpdateOutput[1].indexOf('One or more updates are available') !== -1;
            }
        break;
        case 'status':
            await BashExecution.executeCommand(`arkmanager status${serverName}`, false);
        break;
        default:
            returnValue.success = false;
        break;
    }

    res.send(returnValue);
});

https.createServer({
    key: fs.readFileSync('./server.key'),
    cert: fs.readFileSync('./server.cert')
}, app).listen(port, () => { 
    logger.info('[server] is running.');
});